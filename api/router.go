package api

import (
	"github.com/casbin/casbin/v2"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	_ "gitlab.com/todo-group/api-gateway/api/docs" //swag
	v1 "gitlab.com/todo-group/api-gateway/api/handlers/v1"
	"gitlab.com/todo-group/api-gateway/api/middleware"
	token "gitlab.com/todo-group/api-gateway/api/tokens"
	"gitlab.com/todo-group/api-gateway/config"
	"gitlab.com/todo-group/api-gateway/pkg/logger"
	"gitlab.com/todo-group/api-gateway/services"
	"gitlab.com/todo-group/api-gateway/storage/repo"
)

// Option ...
type Option struct {
	Conf           config.Config
	Logger         logger.Logger
	ServiceManager services.IServiceManager
	Redis          repo.InMemoryStorageI
	CasbinEnforcer *casbin.Enforcer
}

// New ...
// @title           Todo app api
// @version         1.0
// @description     This is api of todo server
// @termsOfService  2 term adds uz

// @contact.name   Azizbek
// @contact.url    https://t.me/azizbek_dev_2005
// @contact.email  azizbekhojimurodov@gmail.com

// @host      54.249.97.210:9090
// @BasePath  /v1

// @securityDefinitions.apikey BearerAuth
// @in header
// @name Authorization
func New(option Option) *gin.Engine {
	router := gin.New()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	jwtHandler := token.JWTHandler{
		SigninKey: option.Conf.SignInKey,
		Log:       option.Logger,
	}

	handlerV1 := v1.New(&v1.HandlerV1Config{
		Logger:         option.Logger,
		ServiceManager: option.ServiceManager,
		Cfg:            option.Conf,
		Redis:          option.Redis,
		JWTHandler:     jwtHandler,
	})

	corConfig := cors.DefaultConfig()
	corConfig.AllowAllOrigins = true
	corConfig.AllowCredentials = true
	corConfig.AllowHeaders = []string{"*"}
	corConfig.AllowBrowserExtensions = true
	corConfig.AllowMethods = []string{"*"}
	router.Use(cors.New(corConfig))

	router.Use(middleware.NewAuth(option.CasbinEnforcer, jwtHandler, config.Load()))
	api := router.Group("/v1")

	// Register user
	api.POST("/user/register", handlerV1.RegisterUser)
	api.GET("/user/verify/:email/:code", handlerV1.VerifyUser)
	api.GET("/user/login", handlerV1.LoginUser)
	api.GET("/user", handlerV1.GetUserPofile)
	api.PUT("/user", handlerV1.UpdateUser)
	api.PUT("/user/profilephoto", handlerV1.SetProfilePhoto)
	api.PUT("/user/password", handlerV1.UpdateUserPassword)
	api.PUT("/user/email", handlerV1.UpdateEmail)
	api.GET("/user/email/verify/:email/:code", handlerV1.VerifyUserEmail)
	api.GET("/user/unique/:username", handlerV1.IsUniqueUsername)
	api.GET("/user/photo", handlerV1.GetProfilePhoto)

	api.POST("/task", handlerV1.ScheduleTask)
	api.PUT("/task", handlerV1.UpdateScheduledTask)
	api.PUT("/task/done/:task_id", handlerV1.DoneTask)
	api.GET("/task", handlerV1.GetAllTasksOfUser)
	api.DELETE("task/:task_id", handlerV1.DeleteTask)
	
	url := ginSwagger.URL("swagger/doc.json")
	api.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
	return router
}
