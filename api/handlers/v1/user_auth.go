package v1

import (
	"context"
	"encoding/json"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/spf13/cast"

	"gitlab.com/todo-group/api-gateway/api/models"
	"gitlab.com/todo-group/api-gateway/genproto/user"
	"gitlab.com/todo-group/api-gateway/pkg/etc"
	"gitlab.com/todo-group/api-gateway/pkg/logger"
)

// Register User
// @Summary 		Register User
// @Description 	Through this api new user can register
// @Tags 			User
// @Accept 			json
// @Produce         json
// @Param           User        body  	  models.UserRegisterReq true "User"
// @Success         202					  {object} 	models.Info
// @Failure         500                   {object}  models.Info
// @Failure         400                   {object}  models.Info
// @Failure         409                   {object}  models.Info
// @Router          /user/register [post]
func (h *handlerV1) RegisterUser(c *gin.Context) {
	body := &models.UserRegisterReq{}
	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, &models.Info{
			Message: "Please check your data",
		})
		h.log.Error("Error while binding json", logger.Error(err))
		return
	}
	body.UserName = strings.TrimSpace(body.UserName)
	body.Email = strings.TrimSpace(body.Email)
	body.Password = strings.TrimSpace(body.Password)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	if e, ok := etc.ValidMailAddress(body.Email); !ok {
		c.JSON(http.StatusBadRequest, &models.Info{
			Message: e + " is not valid mail address",
		})
		return
	}

	isUniqueEmail, err := h.serviceManager.UserService().CheckField(ctx, &user.FieldReq{
		Key:   "email",
		Value: body.Email,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Ooops something went wrong",
		})
		h.log.Error("Error while email uniqueness", logger.Error(err))
		return
	}

	if isUniqueEmail.Exists {
		c.JSON(http.StatusConflict, &models.Info{
			Message: "This email is already registered, please use another email",
		})
		return
	}

	isUniqueUsername, err := h.serviceManager.UserService().CheckField(ctx, &user.FieldReq{
		Key:   "username",
		Value: body.Email,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Ooops something went wrong",
		})
		h.log.Error("Error while username uniqueness", logger.Error(err))
		return
	}

	if isUniqueUsername.Exists {
		c.JSON(http.StatusConflict, &models.Info{
			Message: "This username is already used, please use another username",
		})
		return
	}

	isExistsInRedis, err := h.redis.Exists(body.Email)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Ooops something went wrong",
		})
		h.log.Error("Error while checking email uniqueness from redis", logger.Error(err))
		return
	}

	isExistsInRedisStr := cast.ToString(isExistsInRedis)
	if isExistsInRedisStr == "1" {
		c.JSON(http.StatusConflict, &models.Info{
			Message: "This email is already registered, please use another email or try after 5 minutes",
		})
		return
	}

	userToBeSaved := &user.CreateUserReq{
		Id:        uuid.New().String(),
		FirstName: body.FirstName,
		LastName:  body.LastName,
		Username:  body.UserName,
		Email:     body.Email,
	}

	userToBeSaved.Password, err = etc.HashPassword(userToBeSaved.Password)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Ooops something went wrong",
		})
		h.log.Error("Error while hashing password", logger.Error(err))
		return
	}

	userToBeSaved.Code = etc.GenerateCode(6)

	userByte, err := json.Marshal(userToBeSaved)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Ooops something went wrong",
		})
		h.log.Error("Error while marshaling user", logger.Error(err))
		return
	}

	err = h.redis.SetWithTTL(body.Email, string(userByte), 600)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Ooops something went wrong",
		})
		h.log.Error("Error while saving user to redis", logger.Error(err))
		return
	}

	_, err = h.serviceManager.UserService().SendEmailVerification(ctx, &user.VerifyReq{
		FirstName: body.FirstName,
		Email:     body.Email,
		Code:      userToBeSaved.Code,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Ooops something went wrong",
		})
		h.log.Error("Error while sending verificatin email", logger.Error(err))
		return
	}

	c.JSON(http.StatusAccepted, &models.Info{
		Message: "You have successfully registered to use our services first you have to verify your email, So we have sent you a code",
	})
}

// Verify user email
// @Summary 		Verify user email
// @Description 	Through this api email of user is verified
// @Tags 			User
// @Accept 			json
// @Produce         json
// @Param        	email  		    path      string    true 	"email"
// @Param        	code            path      string    true 	"code"
// @Success         200				{object}  user.CreateUserRes
// @Failure         500             {object}  models.Info
// @Failure         400             {object}  models.Info
// @Failure         409             {object}  models.Info
// @Router          /user/verify/{email}/{code} [get]
func (h *handlerV1) VerifyUser(c *gin.Context) {
	email := c.Param("email")
	code := c.Param("code")
	body := &user.CreateUserReq{}
	email = strings.TrimSpace(email)
	resI, err := h.redis.Get(email)
	if err != nil {
		c.JSON(http.StatusBadRequest, &models.Info{
			Message: "Did you registered first",
		})
		h.log.Error("Error while getting from redis", logger.Error(err))
		return
	}

	resS := cast.ToString(resI)

	err = json.Unmarshal([]byte(resS), &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Did you registered first",
		})
		h.log.Error("Error while unmarshaling from redis res", logger.Error(err))
		return
	}

	if body.Code != code {
		c.JSON(http.StatusConflict, &models.Info{
			Message: "Icorrect code",
		})
		return
	}

	h.jwthandler.Sub = body.Id
	h.jwthandler.Role = "user"
	h.jwthandler.Aud = []string{"todo-frotend"}
	h.jwthandler.SigninKey = h.cfg.SignInKey
	h.jwthandler.Log = h.log
	tokens, err := h.jwthandler.GenerateAuthJWT()
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Info{
			Message: "Ooops something went wrong",
		})
		h.log.Error("Error while creating jwt token", logger.Error(err))
		return
	}

	body.RefreshToken = tokens[1]

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	res, err := h.serviceManager.UserService().CreateUser(ctx, body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Oops something went wrong",
		})
		h.log.Error("Error while creating user", logger.Error(err))
		return
	}
	res.AccessToken = tokens[0]

	c.JSON(http.StatusOK, res)
}

// User login
// @Summary 		Login user
// @Description 	Through this api, user can log in, providing email and password
// @Tags 			User
// @Accept 			json
// @Produce         json
// @Param        	email  		    query     string    true 	"email"
// @Param        	password        query     string    true 	"password"
// @Success         200				{object}  user.GetUserByEmailRes
// @Failure         500             {object}  models.Info
// @Failure         400             {object}  models.Info
// @Failure         409             {object}  models.Info
// @Failure         404             {object}  models.Info
// @Router          /user/login [get]
func (h *handlerV1) LoginUser(c *gin.Context) {
	email := c.Query("email")
	password := c.Query("password")
	email = strings.TrimSpace(email)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	res, err := h.serviceManager.UserService().GetUserByEmail(ctx, &user.GetUserReq{
		Value: email,
	})
	if err != nil {
		c.JSON(http.StatusNotFound, &models.Info{
			Message: "Not found, did you registered before",
		})
		h.log.Error("Error while getting user by email", logger.Error(err))
		return
	}
	if !etc.CheckPasswordHash(password, res.Password) {
		c.JSON(http.StatusConflict, &models.Info{
			Message: "Password incorrect",
		})
		return
	}

	h.jwthandler.Sub = res.Id
	h.jwthandler.Role = "user"
	h.jwthandler.Aud = []string{"todo-frotend"}
	h.jwthandler.SigninKey = h.cfg.SignInKey
	h.jwthandler.Log = h.log
	tokens, err := h.jwthandler.GenerateAuthJWT()
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Info{
			Message: "Ooops something went wrong",
		})
		h.log.Error("Error while creating jwt token", logger.Error(err))
		return
	}
	res.AccessToken = tokens[0]
	res.RefreshToken = tokens[1]

	c.JSON(http.StatusOK, res)
}
