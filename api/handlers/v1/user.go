package v1

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/todo-group/api-gateway/api/models"
	"gitlab.com/todo-group/api-gateway/genproto/user"
	"gitlab.com/todo-group/api-gateway/pkg/logger"
)

// Get user profile
// @Summary 		Get user profile
// @Description 	Through this api email of user is verified
// @Tags 			User
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Success         200				{object}  user.GetUserByIdRes
// @Failure         500             {object}  models.Info
// @Router          /user [get]
func (h *handlerV1) GetUserPofile(c *gin.Context) {
	claim, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Oops something went wrong",
		})
		h.log.Error("Error while getting claims of user", logger.Error(err))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	res, err := h.serviceManager.UserService().GetUserById(ctx, &user.GetUserReq{Value: claim.Sub})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Oops something went wrong",
		})
		h.log.Error("Error while getting user from user service", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, res)
}

// Is uniqueq username
// @Summary 		is unique username
// @Description 	Through this api, whether username is uniqe or not is checked
// @Tags 			User
// @Accept 			json
// @Produce         json
// @Param        	username  	    path      string    true 	"username"
// @Success         200				{object}  models.IsUniqueUsernameRes
// @Failure         500             {object}  models.Info
// @Router          /user/unique/{username} [get]
func (h *handlerV1) IsUniqueUsername(c *gin.Context) {
	username := c.Param("username")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	isUniqueUsername, err := h.serviceManager.UserService().CheckField(ctx, &user.FieldReq{
		Key:   "username",
		Value: username,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Ooops something went wrong",
		})
		h.log.Error("Error while username uniqueness", logger.Error(err))
		return
	}

	c.JSON(http.StatusOK, &models.IsUniqueUsernameRes{
		IsUnique: !isUniqueUsername.Exists,
	})
}

// Get Profile photo of user
// @Summary 		Get profile photo
// @Description 	Through this api, photo of user can be get
// @Tags 			User
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Success 200 	file formData
// @Failure 500 	{object} models.Info
// @Failure 400 	{object} models.Info
// @Router          /user/photo [get]
func (h *handlerV1) GetProfilePhoto(c *gin.Context) {
	claim, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Oops something went wrong",
		})
		h.log.Error("Error while getting claims of user", logger.Error(err))
		return
	}
	fmt.Println(claim.Sub)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	res, err := h.serviceManager.UserService().GetFieldById(ctx, &user.FieldReq{
		Id:  claim.Sub,
		Key: "profile_photo",
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Oops something went wrong",
		})
		h.log.Error("Error while getting profile_photo field of user", logger.Error(err))
		return
	}

	filepath := h.cfg.ImgesPath + res.Value
	fmt.Println(filepath)
	c.Status(http.StatusOK)
	c.File(filepath)
}
