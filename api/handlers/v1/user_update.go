package v1

import (
	"context"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	"gitlab.com/todo-group/api-gateway/api/models"
	"gitlab.com/todo-group/api-gateway/genproto/user"
	"gitlab.com/todo-group/api-gateway/pkg/etc"
	"gitlab.com/todo-group/api-gateway/pkg/logger"
)

// Updated user profile
// @Summary 		Updated user profile
// @Description 	Through this api, user info can be updated
// @Tags 			User
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param           User        body  	  models.UpdateUserReq true "User"
// @Success         200				{object}  user.GetUserByIdRes
// @Failure         500             {object}  models.Info
// @Failure         400             {object}  models.Info
// @Router          /user [put]
func (h *handlerV1) UpdateUser(c *gin.Context) {
	claim, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Oops something went wrong",
		})
		h.log.Error("Error while getting claims of user", logger.Error(err))
		return
	}

	body := &models.UpdateUserReq{}
	err = c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, &models.Info{
			Message: "Please check your data",
		})
		h.log.Error("while binding from request", logger.Error(err))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	res, err := h.serviceManager.UserService().UpdateUser(ctx, &user.UpdateUserReq{
		Id:        claim.Sub,
		FirstName: body.FirstName,
		LastName:  body.LastName,
		Username:  body.UserName,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Oops something went wrong",
		})
		h.log.Error("Error while updating user", logger.Error(err))
		return
	}

	c.JSON(http.StatusOK, res)
}

// Set Profile photo
// @Summary 		Updated user profile
// @Description 	Through this api, user info can be updated
// @Tags 			User
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param         	file     formData file true "File"
// @Success 200 	{object} models.Info
// @Failure 500 	{object} models.Info
// @Failure 400 	{object} models.Info
// @Router          /user/profilephoto [put]
func (h *handlerV1) SetProfilePhoto(c *gin.Context) {
	claim, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Oops something went wrong",
		})
		h.log.Error("Error while getting claims of user", logger.Error(err))
		return
	}

	file := &models.File{}
	err = c.ShouldBind(&file)
	if err != nil {
		c.JSON(http.StatusBadRequest, &models.Info{
			Message: "Please check your data",
		})
		h.log.Error("while binding from request", logger.Error(err))
		return
	}
	ext := filepath.Ext(file.File.Filename)
	if ext != ".jpg" && ext != ".png" {
		c.JSON(http.StatusBadRequest, models.Info{
			Message: "Unsupported file format",
		})
		return
	}

	fileName := claim.Sub + filepath.Ext(file.File.Filename)

	filePath := h.cfg.ImgesPath + fileName
	os.Remove(h.cfg.ImgesPath + claim.Sub + ".jpg")
	os.Remove(h.cfg.ImgesPath + claim.Sub + ".png")

	if err = c.SaveUploadedFile(file.File, filePath); err != nil {
		c.JSON(http.StatusInternalServerError, models.Info{
			Message: "Couldn't set profile image",
		})
		h.log.Error("Error while saving profile photo")
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	_, err = h.serviceManager.UserService().UpdateFieldById(
		ctx,
		&user.FieldReq{
			Id:    claim.Sub,
			Key:   "profile_photo",
			Value: fileName,
		},
	)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Ooops something went wrong",
		})
		h.log.Error("while updating user profile photo", logger.Error(err))
		return
	}

	c.JSON(http.StatusOK, &models.Info{
		Message: "Profile photo successfully updated",
	})
}

// Updated user password
// @Summary 		Updated user password
// @Description 	Through this api, user password can be updated
// @Tags 			User
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param           User        body  	  models.UpdatePasswordReq true "User"
// @Success         200				{object}  models.Info
// @Failure         500             {object}  models.Info
// @Failure         400             {object}  models.Info
// @Router          /user/password [put]
func (h *handlerV1) UpdateUserPassword(c *gin.Context) {
	claim, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Oops something went wrong",
		})
		h.log.Error("Error while getting claims of user", logger.Error(err))
		return
	}

	body := &models.UpdatePasswordReq{}
	err = c.ShouldBindJSON(&body)
	body.NewPassword = strings.TrimSpace(body.NewPassword)
	body.OldPassword = strings.TrimSpace(body.OldPassword)

	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Please check your data",
		})
		h.log.Error("Error while unmarshaling request", logger.Error(err))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	res, err := h.serviceManager.UserService().GetFieldById(ctx, &user.FieldReq{
		Id:  claim.Sub,
		Key: "password",
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Please check your data",
		})
		h.log.Error("Error while getting password of user", logger.Error(err))
		return
	}

	if !etc.CheckPasswordHash(body.OldPassword, res.Value) {
		c.JSON(http.StatusBadRequest, &models.Info{
			Message: "Old password is incorrect",
		})
		return
	}

	body.NewPassword, err = etc.HashPassword(body.NewPassword)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Please check your data",
		})
		h.log.Error("Error while hashing password of user", logger.Error(err))
		return
	}

	_, err = h.serviceManager.UserService().UpdateFieldById(ctx, &user.FieldReq{
		Id:    claim.Sub,
		Key:   "password",
		Value: body.NewPassword,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Please check your data",
		})
		h.log.Error("Error while hashing password of user", logger.Error(err))
		return
	}

	c.JSON(http.StatusOK, &models.Info{
		Message: "Password successfully upated",
	})
}

// Update userEmail
// @Summary 		Updated user email
// @Description 	Through this api, user email can be updated
// @Tags 			User
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param           Email        body  	  models.UpdateEmailReq true "Email"
// @Success         200				{object}  models.Info
// @Failure         500             {object}  models.Info
// @Failure         400             {object}  models.Info
// @Router          /user/email [put]
func (h *handlerV1) UpdateEmail(c *gin.Context) {
	claim, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Oops something went wrong",
		})
		h.log.Error("Error while getting claims of user", logger.Error(err))
		return
	}

	body := &models.UpdateEmailReq{}
	err = c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Please check your data",
		})
		h.log.Error("Error while unmarshaling request", logger.Error(err))
		return
	}
	body.Email = strings.TrimSpace(body.Email)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	isUniqueEmail, err := h.serviceManager.UserService().CheckField(ctx, &user.FieldReq{
		Key:   "email",
		Value: body.Email,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Ooops something went wrong",
		})
		h.log.Error("Error while email uniqueness", logger.Error(err))
		return
	}

	if isUniqueEmail.Exists {
		c.JSON(http.StatusConflict, &models.Info{
			Message: "This email is already registered, please use another email",
		})
		return
	}

	isExistsInRedis, err := h.redis.Exists(body.Email)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Ooops something went wrong",
		})
		h.log.Error("Error while checking email uniqueness from redis", logger.Error(err))
		return
	}

	isExistsInRedisStr := cast.ToString(isExistsInRedis)
	if isExistsInRedisStr == "1" {
		c.JSON(http.StatusConflict, &models.Info{
			Message: "This email is already registered, please use another email or try after 5 minutes",
		})
		return
	}

	userRes, err := h.serviceManager.UserService().GetFieldById(ctx, &user.FieldReq{
		Id:  claim.Sub,
		Key: "first_name",
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Ooops something went wrong",
		})
		h.log.Error("Error while getting firstName", logger.Error(err))
		return
	}

	code := etc.GenerateCode(6)

	err = h.redis.SetWithTTL(body.Email, code, 300)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Ooops something went wrong",
		})
		h.log.Error("Error while saving code into redis", logger.Error(err))
		return
	}

	_, err = h.serviceManager.UserService().SendEmailVerification(ctx, &user.VerifyReq{
		FirstName: userRes.Value,
		Email:     body.Email,
		Code:      code,
	})

	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Ooops something went wrong",
		})
		h.log.Error("Error while sending verification code", logger.Error(err))
		return
	}

	c.JSON(http.StatusOK, &models.Info{
		Message: "To verify your email we have sent you a code",
	})
}

// Verify user email
// @Summary 		Verify user email
// @Description 	Through this api email of user is verified
// @Tags 			User
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param        	email  		    path      string    true 	"email"
// @Param        	code            path      string    true 	"code"
// @Success         200				{object}  models.Info
// @Failure         500             {object}  models.Info
// @Failure         400             {object}  models.Info
// @Failure         409             {object}  models.Info
// @Router          /user/email/verify/{email}/{code} [get]
func (h *handlerV1) VerifyUserEmail(c *gin.Context) {
	claim, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Oops something went wrong",
		})
		h.log.Error("Error while getting claims of user", logger.Error(err))
		return
	}

	email := c.Param("email")
	code := c.Param("code")
	email = strings.TrimSpace(email)
	code = strings.TrimSpace(code)

	resI, err := h.redis.Get(email)
	if err != nil {
		c.JSON(http.StatusBadRequest, &models.Info{
			Message: "Did you registered first",
		})
		h.log.Error("Error while getting from redis", logger.Error(err))
		return
	}

	resS := cast.ToString(resI)

	if resS != code {
		c.JSON(http.StatusConflict, &models.Info{
			Message: "Icorrect code",
		})
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	res, err := h.serviceManager.UserService().UpdateFieldById(ctx, &user.FieldReq{
		Id:    claim.Sub,
		Key:   "email",
		Value: email,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Oops something went wrong",
		})
		h.log.Error("Error while creating user", logger.Error(err))
		return
	}

	c.JSON(http.StatusOK, res)
}
