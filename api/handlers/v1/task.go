package v1

import (
	"context"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/todo-group/api-gateway/api/models"
	"gitlab.com/todo-group/api-gateway/genproto/task"
	"gitlab.com/todo-group/api-gateway/pkg/logger"
)

// Schedule task
// @Summary 		Schedule new task
// @Description 	Through this api new task can be scheduled.
// @Tags 			Task
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param           User        body  	  models.ScheduleTaskReq true "User"
// @Success         200					  {object} 	models.Info
// @Failure         500                   {object}  models.Info
// @Failure         400                   {object}  models.Info
// @Router          /task [post]
func (h *handlerV1) ScheduleTask(c *gin.Context) {
	claim, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Oops something went wrong",
		})
		h.log.Error("Error while getting claims of user", logger.Error(err))
		return
	}

	body := &models.ScheduleTaskReq{}

	err = c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Oops something went wrong",
		})
		h.log.Error("Error while binding json", logger.Error(err))
		return
	}

	taskTobeSecudeled := &task.ScheduleReq{
		Id:          uuid.New().String(),
		UserId:      claim.Sub,
		Title:       body.Title,
		Description: body.Description,
		StartTime:   body.StartTime,
		FinishTime:  body.FinishTime,
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	res, err := h.serviceManager.TaskService().ScheduleTask(ctx, taskTobeSecudeled)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Oops something went wrong",
		})
		h.log.Error("Error while scheduling task", logger.Error(err))
		return
	}

	c.JSON(http.StatusOK, res)
}

// Update Scheduled task
// @Summary 		update Scheduled task
// @Description 	Through this api, scheduled task can be updated.
// @Tags 			Task
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param           User        body  	  models.UpdateScheduledTaskReq true "User"
// @Success         200					  {object} 	task.ScheduleRes
// @Failure         500                   {object}  models.Info
// @Failure         400                   {object}  models.Info
// @Router          /task [put]
func (h *handlerV1) UpdateScheduledTask(c *gin.Context) {
	claim, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Oops something went wrong",
		})
		h.log.Error("Error while getting claims of user", logger.Error(err))
		return
	}

	body := &models.UpdateScheduledTaskReq{}

	err = c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, &models.Info{
			Message: "Oops something went wrong",
		})
		h.log.Error("Error while binding json", logger.Error(err))
		return
	}

	taskTobeSecudeled := &task.ScheduleReq{
		Id:          body.Id,
		UserId:      claim.Sub,
		Title:       body.Title,
		Description: body.Description,
		StartTime:   body.StartTime,
		FinishTime:  body.FinishTime,
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	res, err := h.serviceManager.TaskService().UpdateTask(ctx, taskTobeSecudeled)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Oops something went wrong",
		})
		h.log.Error("Error while updating task", logger.Error(err))
		return
	}

	c.JSON(http.StatusOK, res)
}

// Get All Tasks Of user
// @Summary 		Get all tasks of user
// @Description 	Through this api, all tasks of user is taken
// @Tags 			Task
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Success         200					  {object} 	task.GetAllTasksRes
// @Failure         500                   {object}  models.Info
// @Router          /task [get]
func (h *handlerV1) GetAllTasksOfUser(c *gin.Context) {
	claim, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Oops something went wrong",
		})
		h.log.Error("Error while getting claims of user", logger.Error(err))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	res, err := h.serviceManager.TaskService().GetAllTasks(ctx, &task.OneFieldReq{
		Value: claim.Sub,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Oops something went wrong",
		})
		h.log.Error("Error while getting all tasks", logger.Error(err))
		return
	}

	c.JSON(http.StatusOK, res)
}

// Make the task done
// @Summary 		Make the task done
// @Description 	Through this api, a task is marked done
// @Tags 			Task
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param        	task_id        path     string    true 	"task_id"
// @Success         200					  {object} 	task.GetAllTasksRes
// @Failure         500                   {object}  models.Info
// @Router          /task/done/{task_id} [put]
func (h *handlerV1) DoneTask(c *gin.Context) {
	id := c.Param("task_id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	_, err := h.serviceManager.TaskService().DoneTask(ctx, &task.OneFieldReq{Value: id})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Oops something went wrong",
		})
		h.log.Error("Error while marking task done", logger.Error(err))
		return
	}

	c.JSON(http.StatusOK, &models.Info{
		Message: "The task is successfully marked done",
	})
}

// Delete task
// @Summary 		Delete task
// @Description 	Through this api, a task is deleted
// @Tags 			Task
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param        	task_id        path     string    true 	"task_id"
// @Success         200					  {object} 	models.Info
// @Failure         500                   {object}  models.Info
// @Router          /task/{task_id} [delete]
func (h *handlerV1) DeleteTask(c *gin.Context) {
	id := c.Param("task_id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	_, err := h.serviceManager.TaskService().DeleteTask(ctx, &task.OneFieldReq{Value: id})
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.Info{
			Message: "Oops something went wrong",
		})
		h.log.Error("Error while deleting task", logger.Error(err))
		return
	}

	c.JSON(http.StatusOK, &models.Info{
		Message: "The task is deleted",
	})
}
