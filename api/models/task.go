package models

type ScheduleTaskReq struct {
	Title       string `json:"title" binding:"required" example:"title"`
	Description string `json:"description" binding:"required" example:"description"`
	StartTime   string `json:"start_time" binding:"required" example:"2022-12-04T17:36:56+05:00"`
	FinishTime  string `json:"finish_time"  binding:"required" example:"2022-12-04T17:36:56+05:00"`
}

type UpdateScheduledTaskReq struct {
	Id          string `json:"id" binding:"required"`
	Title       string `json:"title" binding:"required" example:"title"`
	Description string `json:"description" binding:"required" example:"description"`
	StartTime   string `json:"start_time" binding:"required" example:"2022-12-04T17:36:56+05:00"`
	FinishTime  string `json:"finish_time"  binding:"required" example:"2022-12-04T17:36:56+05:00"`
}
