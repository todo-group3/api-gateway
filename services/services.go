package services

import (
	"fmt"

	"gitlab.com/todo-group/api-gateway/config"
	"gitlab.com/todo-group/api-gateway/genproto/task"
	"gitlab.com/todo-group/api-gateway/genproto/user"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type IServiceManager interface {
	UserService() user.UserServiceClient
	TaskService() task.TaskServiceClient
}

type serviceManager struct {
	userService user.UserServiceClient
	taskService task.TaskServiceClient
}

func NewServiceManager(conf *config.Config) (IServiceManager, error) {
	userServiceConn, err := grpc.Dial(
		fmt.Sprintf("%s:%d", conf.UserServiceHost, conf.UserServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	taskServiceConn, err := grpc.Dial(
		fmt.Sprintf("%s:%d", conf.TaskServiceHost, conf.TaskServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}
	
	serviceManager := &serviceManager{
		userService: user.NewUserServiceClient(userServiceConn),
		taskService: task.NewTaskServiceClient(taskServiceConn),
	}

	return serviceManager, nil
}

func (s *serviceManager) UserService() user.UserServiceClient {
	return s.userService
}

func (s *serviceManager) TaskService() task.TaskServiceClient {
	return s.taskService
}
