run:
	go run cmd/main.go

proto-gen:
	./script/gen-proto.sh
	
add-submodule:
	git submodule add git@gitlab.com:todo-group3/protos.git

update-submodule:
	git submodule update --remote --merge

swag:
	swag init -g api/router.go -o api/docs