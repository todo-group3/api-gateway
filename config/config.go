package config

import (
	"os"

	"github.com/spf13/cast"
)

// Config ...
type Config struct {
	Environment     string // develop, staging, production
	UserServiceHost string
	UserServicePort int
	TaskServiceHost string
	TaskServicePort int
	CtxTimeout      int // context timeout in seconds
	LogLevel        string
	HTTPPort        string
	RedisHost       string
	RedisPort       string
	SignInKey       string
	AuthConfigPath  string
	CSVFilePath     string
	ImgesPath       string
}

// Load loads environment vars and inflates Config
func Load() Config {
	c := Config{}

	c.Environment = cast.ToString(GetOrReturnDefault("ENVIRONMENT", "develop"))
	c.LogLevel = cast.ToString(GetOrReturnDefault("LOG_LEVEL", "debug"))
	c.HTTPPort = cast.ToString(GetOrReturnDefault("HTTP_PORT", "9090"))

	c.UserServiceHost = cast.ToString(GetOrReturnDefault("USER_SERVICE_HOST", "localhost"))
	c.UserServicePort = cast.ToInt(GetOrReturnDefault("USER_SERVICE_PORT", 9999))

	c.TaskServiceHost = cast.ToString(GetOrReturnDefault("TASK_SERVICE_HOST", "localhost"))
	c.TaskServicePort = cast.ToInt(GetOrReturnDefault("TASK_SERVICE_PORT", 9998))

	c.RedisHost = cast.ToString(GetOrReturnDefault("REDIS_HOST", "localhost"))
	c.RedisPort = cast.ToString(GetOrReturnDefault("REDIS_PORT", "6379"))

	c.SignInKey = cast.ToString(GetOrReturnDefault("SIGNINGKEY", "AzizbekSignIn"))
	c.CtxTimeout = cast.ToInt(GetOrReturnDefault("CTX_TIMEOUT", 7))
	c.AuthConfigPath = cast.ToString(GetOrReturnDefault("AUTH_PATH", "./config/auth.conf"))
	c.CSVFilePath = cast.ToString(GetOrReturnDefault("CSV_FILE_PATH", "./config/auth.csv"))
	c.ImgesPath = cast.ToString(GetOrReturnDefault("IMAGES_PATH", "./storage/images/"))

	return c
}

func GetOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
