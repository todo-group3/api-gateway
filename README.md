# To do api gateway
Bu yerda client side uchun REST API chiqariladi. 

# Tools
1. gRPC
2. Swagger
3. Casbin 
4. JWT
5. Docker

# Run
```
docker compose up
```
 
I would recommend you to check logs of email service. If the there is log that says to restart the service, run the following command. 

``` 
docker restart email
```


